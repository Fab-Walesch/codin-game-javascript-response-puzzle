# codin game javascript response puzzle

Hello,

I'm Fabien Walesch
I have been a javascript developer for 2 years and a half.

These puzzle resolutions should just serve as a guide if you are stuck.
Before copying the code, search for the solution yourself ;)

Have fun and clean code first ...

If you want to contact me : fabien.walesch@gmail.com 

Bye, see you soon,

For use just copy the code to codin game solution :

   https://www.codingame.com/home