const L = parseInt(readline())
const H = parseInt(readline())
const T = readline()

let letters = T.toUpperCase().split('')

let alphaArray = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ?'.split('')
let alphaMap = {}

for (let j = alphaArray.length - 1; j >= 0; j--) {
    alphaMap[alphaArray[j]] = []
}

let asciiArray = []

for (let i = 0; i < H; i++) {
    
    let row = readline()
    let currentLetterIndex = 0
    
    for (let j = 0; j < alphaArray.length; j++) {
        let thisLetter = alphaArray[j]
        alphaMap[thisLetter][i] = row.substr(currentLetterIndex, L)
        currentLetterIndex += L
    }
}

let result = ''

for (let j = 0; j < H; j++) {
    for (let i = 0; i < letters.length; i++) {
        let thisLetter = letters[i];
        if ((thisLetter < 'A') || (thisLetter > 'Z')) {
            thisLetter = '?'
        }
        result += alphaMap[thisLetter][j]
    }
    result += '\n'
}


print(result)



