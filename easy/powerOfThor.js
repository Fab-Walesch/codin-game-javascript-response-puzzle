let inputs = readline().split(' ')
let lightXPos = parseInt(inputs[0])
let lightYPos = parseInt(inputs[1]) 
let initialThorX = parseInt(inputs[2]) 
let initialThorY = parseInt(inputs[3]) 
let thorXPos = initialThorX
let thorYPos = initialThorY

while (true) {
    const n = parseInt(readline())
    let remainingTurns = n 
    let dirX = ''
    let dirY = ''

    if (thorXPos > lightXPos){
        dirX = 'W'
        thorXPos --
    } else if (thorXPos < lightXPos) {
        dirX = 'E'
        thorXPos ++
    } else { 
        dirX = ''
    }
    
    if(thorYPos > lightYPos){
        dirY = 'N'
        thorYPos --
    } else if (thorYPos < lightYPos) {
        dirY = 'S'
        thorYPos ++
    } else {
        dirY = ''
    }
    
    print(dirY + dirX)

}