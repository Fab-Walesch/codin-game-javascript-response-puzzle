const n = parseInt(readline());
let tmps = readline();

let result = 10000;
tmps = tmps.split(/\s+/).map((n) => {
    return parseInt(n);
});

if(!n){
    print(0);
} else {
    for (let i = n-1; i >= 0; i--) {
        let thisTmp = tmps[i];
        if(Math.abs(thisTmp) < Math.abs(result)){
               result = thisTmp;
        }
        else if(Math.abs(thisTmp) === Math.abs(result)){
               result = (result > thisTmp) ? result : thisTmp;
        }
    }
    print(result);
}
